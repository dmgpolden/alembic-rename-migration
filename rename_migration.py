from datetime import datetime
from io import TextIOWrapper
from os import listdir, rename
from pathlib import Path

"""
    Add file_template in [alembic]
    file_template = %%(year)d%%(month).2d%%(day).2d-%%(hour).2d%%(minute).2d%%(second).2d_%%(rev)s_%%(slug)s
    for example:

# A generic, single database configuration.

[alembic]
# path to migration scripts
script_location = migrations
file_template = %%(year)d%%(month).2d%%(day).2d-%%(hour).2d%%(minute).2d%%(second).2d_%%(rev)s_%%(slug)s
"""

def get_created_at(file_data: TextIOWrapper) -> datetime:
    for line in file_data.readlines():
        if line.startswith('Create Date:'):
            created_at = line.replace('Create Date: ', '').split('.')[0]
            return datetime.fromisoformat(created_at)
    raise Exception('Error file')


def get_new_name(filename: str, created_at: datetime) -> str:
    return created_at.strftime('%Y%m%d-%H%M%S') + '_' + filename


def renamer(path, filename):
    print(filename)
    with open(Path(path, filename)) as f:
        created_at = get_created_at(f)
    newname = get_new_name(filename, created_at)
    print(newname)
    rename(Path(path, filename), Path(path, newname))


def main(path):
    for file in listdir(path):
        if file.endswith('.py'):
            renamer(path, file)


if __name__ == '__main__':
    main(path='migrations/versions/')
